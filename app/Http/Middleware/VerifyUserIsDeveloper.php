<?php

namespace App\Http\Middleware;

use App\Imager\Imager;

class VerifyUserIsDeveloper
{
    /**
     * Determine if the authenticated user is a developer.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return \Illuminate\Http\Response
     */
    public function handle($request, $next)
    {
        if ($request->user() && Imager::developer($request->user()->email)) {
            return $next($request);
        }
        
        abort(404);
    }
}
