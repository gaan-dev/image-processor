<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Property;

class ImagerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'auth.developer']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $properties = Property::all();
        return view('imager.index', compact('properties'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'address'   =>  'required',
            'url'   =>  'required'
        ]);
        Property::create($request->toArray());
    }
}
