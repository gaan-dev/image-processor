<?php

namespace App\Imager;

class Imager{
	public static $developers = [];

	public static function developer($email){
		if(in_array($email, static::$developers)){
			return true;
		}

		foreach(static::$developers as $developer) {
			if(str_is($developer, $email)){
				return true;
			}
		}
		return false;
	}		

	public static function developers(array $developers)
	{
		static::$developers = $developers;
	}
}

