<?php

namespace App\Providers;

use App\Imager\Imager;
use Illuminate\Support\ServiceProvider;

class ImagerServiceProvider extends ServiceProvider
{
    protected $developers = [
        'sean@isgoms.com',
        'scgoms@gmail.com'
    ];
    
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        if(count($this->developers) > 0) {
            Imager::developers($this->developers);
        }
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
