@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-2">
            <div class="card">
                <div class="card-header">
                    Floorplans
                </div>
                @if(count($properties) > 0)
                <ul class="list-group">
                    @foreach($properties as $property)
                        <li class="list-group-item">
                            <a href="#" @click.prevent="fireEvent('showProperty', {{ $property }})">
                                {{ $property->address }}
                            </a>
                        </li>
                    @endforeach
                </ul>
                @else 
                <div class="card-body">
                    No properties found yet
                </div>
                @endif
                <div class="card-footer">
                    <button class="btn btn-primary rounded-circle float-right" style="width: 40px; height: 40px" data-toggle="modal" data-target="#upload-form">
                        <span class="fa fa-plus"></span>
                    </button>
                    @include('imager.partials.upload-form')
                </div>
            </div>
        </div>
        <div class="col-md-10">
            <property-card></property-card>
        </div>
    </div>
</div>
<div id="container"></div>
@endsection
