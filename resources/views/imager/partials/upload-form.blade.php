<div class="modal fade" id="upload-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<imager-form method="POST" action="/properties" id="upload-form" inline-template>
	<form @submit.prevent="submitForm" >
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Property Creation</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<!-- Form content -->
					<div class="form-group">
						<label for="address">Address</label>
						<input type="text" class="form-control" name="address" placeholder="Address">
					</div>
					<div class="form-group">
						<label for="address">Floorplan URL</label>
						<input type="text" class="form-control" name="url" placeholder="Floorplan URL">
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary" :disabled="submitting">
					<div v-if="submitting">
						<i class="fa fa-spinner fa-spin fa-fw"></i>
						<span class="sr-only">Loading...</span>
						Creating
					</div>
					<div v-else>
						Save changes
					</div>
					</button>
				</div>
			</div>
		</div>
	</form>
	</imager-form>
</div>